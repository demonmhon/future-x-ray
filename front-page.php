<?php get_header(); ?>
<div class="row">
    <!-- Main content with Sidebar -->
    <div class="col-xs-12 col-md-9 main-content-body">
        <div class="row">
            <div class="main-content col-md-8">
                   <!-- Frontpage Content -->
                <div class="row frontpage-content">
                    <div class="col-md-12">
                        <?php
                            $front_page_post = get_posts(
                                array(
                                    'name'      => THEME_CONFIG_PAGE_HOME,
                                    'post_type' => 'page'
                                )
                            );
                            $front_page = $front_page_post[0];
                        ?>
                        <p><?php echo $front_page->post_content; ?></p>
                    </div>
                </div>
            </div>
            <!-- Sub content -->
            <?php get_sidebar(); ?>
        </div>
    </div>
    <?php get_template_part('main-nav') ?>
</div>
<?php get_footer(); ?>