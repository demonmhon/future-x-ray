        </div>
    </div>
    <div id="footer" class="footer">
        <div class="container">
            <div class="footer-nav row">
                <div class="col-xs-12">
                   <?php
                        $nav_menu = 
                            array(
                                'theme_location' => THEME_CONFIG_FOOTER_MENU_LOCATION,
                                'container'      => THEME_CONFIG_FOOTER_MENU_CONTAINER,
                                'menu_id'        => THEME_CONFIG_FOOTER_MENU_ID,
                                'menu_class'     => THEME_CONFIG_FOOTER_MENU_CLS,
                                'depth'          => THEME_CONFIG_FOOTER_MENU_DEPTH
                            );
                        wp_nav_menu($nav_menu);
                    ?>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-8 company-info">
                    <p class="copy">copyright&copy;2015 Future Decoration Group Co,Ltd</p>
                    <div class="group">
                        <p class="bold company-name">บริษัท ฟิวเจอร์เดคคอเรชั่นกรุ๊ป จำกัด</p>
                        <p>5/165 Mu 2 T.sarongnue A. mung  ,Samutprakan  10270</p>
                    </div>
                    <div class="group">
                        <p class="bold hot-line">Tel: <a href="tel:+6621863331">02-186-3331</a> / Fax: <a href="tel:+6621863332">02-186-3332</a> / Mobile: <a href="tel:+66819388366"> 081-938-8366</a></p>
                        <p>Email: <a href="mailto:wanwanich4145@gmail.com">wanwanich4145@gmail.com</a></p>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <div class="row">
                        <div class="col-xs-6 col-sm-12">
                            <div class="logo">
                                <img src="<?php bloginfo('template_url'); ?>/img/logo/main-logo.png" alt="Future X-Ray Logo" class="img-responsive">
                            </div>
                        </div>
                        <div class="col-xs-6 col-sm-12">
                            <ul class="site-social nav navbar-nav">
                                <li class="rss">
                                    <a href="#" title="RSS Feed" title="Subscribe Future X-ray RSS Feed"><i class="fa fa-rss-square"></i></a>
                                    </li>
                                <li class="youtube">
                                    <a href="https://www.youtube.com/channel/UCnxDhBctT0oOYeQn_1D07_w" title="Youtube" title="Future X-ray YouTube Chanel"><i class="fa fa-youtube-play"></i></a>
                                    </li>
                                <li class="facebook">
                                    <a href="#" title="Facebook" title="Future X-ray Facebook Page"><i class="fa fa-facebook-square"></i></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php wp_footer(); ?>
</body>
</html>