<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <?php if (is_home() || is_front_page()) : ?>
    <title><?php bloginfo('name'); ?> - <?php bloginfo( 'description' ); ?></title>
    <?php else: ?>
    <title><?php bloginfo('name'); ?> - <?php wp_title(' '); ?></title>
    <?php endif; ?>

    <!-- Document Metadata -->
    <meta charset="<?php bloginfo( 'charset' ); ?>" />

    <!-- Style Sheets -->
    <link rel="shortcut icon" href="<?php bloginfo('template_url'); ?>/favicon.ico" />
    <link href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,700|Open+Sans:400italic,600,400' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/global.all.min.css" />

    <!-- JavaScript -->
    <?php wp_enqueue_script('theme_js_script'); ?>
    <?php
    if (is_singular() && get_option('thread_comments')) { wp_enqueue_script('comment-reply'); }
    wp_head();
    ?>
</head>
<body <?php body_class(); ?>>
    <div id="header" class="header">
        <nav class="navbar" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main-site-nav" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="<?php bloginfo('url'); ?>">
                        <span class="logo"><img src="<?php bloginfo('template_url'); ?>/img/logo/main-logo.png" alt="Future X-Ray logo" class="img-responsive"></span>
                        <span class="text"><?php bloginfo('name'); ?></span>
                    </a>
                </div>
                <div class="tagline visible-lg">
                    <span class="tagline-1"></span>
                    <span class="tagline-2"></span>
                </div>
                <div id="main-site-nav" class="main-site-nav collapse navbar-collapse">
                    <!-- Main menu / site navigation -->
                    <?php
                        $nav_menu =
                            array(
                                'theme_location' => THEME_CONFIG_MAIN_MENU_LOCATION,
                                'container'	  => THEME_CONFIG_MAIN_MENU_CONTAINER,
                                'menu_id'		=> THEME_CONFIG_MAIN_MENU_ID,
                                'menu_class'	 => THEME_CONFIG_MAIN_MENU_CLS,
                                'depth'		  => THEME_CONFIG_MAIN_MENU_DEPTH,
                                'walker'		 => new wp_bootstrap_navwalker()
                            );
                        wp_nav_menu($nav_menu);
                    ?>
                    <!-- Language -->
                    <ul class="language-nav navbar-nav nav">
                        <li><a href="<?php bloginfo('url'); ?>">ภาษาไทย</a></li>
                        <li><a href="<?php bloginfo('url'); ?>/en">English</a></li>
                    </ul>
                    <!-- Search box -->
                    <form class="site-search navbar-form" role="search" action="<?php echo home_url( '/' ); ?>">
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Search" name="s">
                        </div>
                        <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                    </form>
                </div>
            </div>
        </nav>
    </div>
    <div id="content" class="document-content">
        <div class="container">
        <?php if (is_home() || is_front_page()) : ?>
            <?php
                $index_bannner = query_posts(
                    array(
                        category_name  => THEME_CONFIG_CAT_INDEX_BANNER,
                        posts_per_page => THEME_CONFIG_LIMIT_INDEX_BANNER
                    )
                );
                $post_count = $wp_query->found_posts;
                if (have_posts()):
                    include( locate_template('img-banner.php') );
                endif;
            ?>
        <?php endif ?>
        <div class="row hotline-bar hidden-xs">
            <div class="col-xs-12">
                <span class="hotline-item phone"><a href="tel:+66819388366"><i class="fa fa-phone-square"></i> Hot Line : 08-1938-8366</a></span>
                <span class="hotline-item mail"><a href="maito:wanwanich4145@gmail.com"><i class="fa fa-envelope"></i> wanwanich4145@gmail.com</a></span>
                <span class="hotline-item line"><i class="fa fa-comment"></i>Line: wanwanich_interior </span>
            </div>
        </div>
