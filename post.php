<div class="row post-container">
    <div class="col-md-12">
        <?php if (is_home() || is_front_page()) : ?>
            <h2 class="title"><?php bloginfo('title'); ?></h2>
        <?php elseif (is_single() || is_page()) : ?>
            <h1 class="title"><?php the_title(); ?></h1>
        <?php else: ?>
            <h2 class="title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
        <?php endif; ?>
        <div class="the-info">
            <?php if (!is_home() && !is_front_page()) : ?>
                <p class="the-date hide">วันที่โพสต์:  <span class="date"><?php the_date('M d, Y'); ?></span></p>
                <p class="the-tags hide"><?php the_tags('แท็ก: '); ?></p>
            <?php endif ?>
        </div>
        <div class="the-content">
            <?php the_content(); ?>
        </div>
    </div>
</div>

<?php if (is_single() && is_admin()) : ?>
<!-- Admin content tools -->
<div class="row admin-content-tools">
    <div class="col-md-8">
        <p class="comment-link"><span class="count"><?php comments_popup_link('No Comment','1 Comment','% Comments'); ?></span></p>
        <p class="edit-link"><span><?php edit_post_link('Edit','',''); ?></span></p>
    </div>
</div>
<?php endif; ?>