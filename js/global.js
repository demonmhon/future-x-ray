var app;

(function($){
var App = function () {
    return this.init();
};

App.prototype.init = function () {
    var self = this;

    self.ui();
    self.disableRightClick();
    self.navMenu();

    return self;
};

App.prototype.disableRightClick = function () {
    $(document).on( 'contextmenu', function (e) {
        if( e.button == 2 ) {
          return false;
        }
    });
};

App.prototype.navMenu = function () {
    var self = this;
    var subMenu = $( '#main-menu > li .dropdown-menu' ).find( '.dropdown-menu' );
    $.each( subMenu, function( i, menu) {
        $( menu ).parent().addClass('dropdown-submenu');
    });
};

App.prototype.ui = function () {

    $('.main-content-body img').addClass('img-responsive');
    var contactForm = $('.contact-form');
    contactForm.find('input[type="text"]').addClass('form-control');
    contactForm.find('input[type="email"]').addClass('form-control');
    contactForm.find('textarea').addClass('form-control');
    contactForm.find('input[type="submit"]').addClass('btn btn-primary');
};

$(document).ready( function () { app = new App (); } );
})(jQuery);
