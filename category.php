<?php get_header(); ?>
<div class="row post-category">
    <div class="col-xs-12 col-md-9 main-content-body">
        <?php
            $page_width_class = 'col-md-8';
        ?>
        <div class="row">
            <div id="category-<?php the_ID(); ?>" class="<?php echo $page_width_class; ?>">
                <div class="row catagory-title">
                    <div class="category-title col-md-12">
                        <h1 class="title"><?php single_cat_title(); ?></h1>
                    </div>
                </div>
                <?php if (have_posts()): ?>
                    <?php get_template_part('list', 'post-thumbnail'); ?>
                <?php endif; ?>
                <div class="row post-pagination">
                    <div class="col-md-offset-1 col-md-6">
                        <span class="entries prev-entries pull-right"><?php next_posts_link('หน้าถัดไป'); ?></span>
                        <span class="entries next-entries pull-left"><?php previous_posts_link('หน้าก่อนหน้า'); ?></span>
                    </div>
                </div>
            </div>
            <?php get_sidebar(); ?>
        </div>
    </div>
    <?php get_template_part('main-nav') ?>
</div>
<?php get_footer(); ?>