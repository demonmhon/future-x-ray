<!-- Banner Slide -->
<div class="row">
    <div class="carousel-wrapper">
        <div id="index-slide" class="index-carousel carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                <?php $item = 0; ?>
                <?php while (have_posts()) : the_post(); ?>
                    <li data-target="#index-slide" data-slide-to="<?php echo $item; ?>"<?php if ($item == 0) : ?>  class="active"<?php endif; ?>></li>
                    <?php $item++ ?>
                <?php endwhile; ?>
            </ol>
            <div class="carousel-inner" role="listbox">
                <?php $item = 1; ?>
                    <?php while (have_posts()) : the_post(); ?>
                    <div class="item<?php if ($item == 1) : ?> active<?php endif; ?>">
                        <?php the_post_thumbnail('full'); ?>
                        <div class="carousel-caption">
                            <span class="small">Furture X-ray</span>
                            <span><?php if ( has_excerpt() ) : ?><?php the_excerpt() ?><?php else : ?>Radiation Protection Expert<?php endif ?></span>
                        </div>
                    </div>
                    <?php $item++ ?>
                <?php endwhile; ?>
            </div>
            <a class="left carousel-control" href="#index-slide" role="button" data-slide="prev">
                <!--
                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
                -->
            </a>
            <a class="right carousel-control" href="#index-slide" role="button" data-slide="next">
                <!--
                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
                -->
            </a>
        </div>
    </div>
</div>