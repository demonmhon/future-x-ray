<!-- Main navigation -->
<div class="col-md-3 col-xs-12 main-navigation">
    <div class="row">
        <div class="col-xs-12">
            <?php
                $page_args               = array('name' => THEME_CONFIG_CAT_SERVICES, 'post_type' => 'page');
                $services_main_page      = get_posts($page_args);
                $services_main_page_item = $services_main_page[0];
                $sub_page_args           = array('child_of' => $services_main_page_item->ID, 'sort_column' => 'menu_order');
                $services_sub_page       = get_pages($sub_page_args);
            ?>
            <h2 class="title"><?php echo $services_main_page_item->post_title ?></h2>
            <ul class="nav" role="navigation">
            <?php foreach($services_sub_page as $page) : ?>
                <li>
                    <a href="<?php echo get_page_link( $page->ID ) ?>"><?php echo $page->post_title ?></a>
                </li>
            <?php endforeach ?>
            </ul>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <?php
                $page_args               = array('name' => THEME_CONFIG_CAT_PRODUCTS, 'post_type' => 'page');
                $products_main_page      = get_posts($page_args);
                $products_main_page_item = $products_main_page[0];
                $sub_page_args           = array('child_of' => $products_main_page_item->ID, 'sort_column' => 'menu_order');
                $products_sub_page       = get_pages($sub_page_args);
            ?>
            <h2 class="title"><?php echo $products_main_page_item->post_title ?></h2>
            <ul class="nav" role="navigation">
            <?php foreach($products_sub_page as $page) : ?>
                <li>
                    <a href="<?php echo get_page_link( $page->ID ) ?>"><?php echo $page->post_title ?></a>
                </li>
            <?php endforeach ?>
        </div>
    </div>
    <!-- Widget -->
    <div class="sidebar">
        <div class="widget row">
           <div class="col-xs-12">
                <div class="body banner">
                    <?php
                        $page_args     = array('name' => THEME_CONFIG_PAGE_CONTACT, 'post_type' => 'page');
                        $contact_page  = get_posts($page_args);
                        $contact_page  = $contact_page[0];
                    ?>
                    <a href="<?php echo get_page_link( $contact_page->ID ) ?>">
                        <img src="<?php bloginfo('template_url'); ?>/img/banner/contact-us.png" alt="Contact us banner" class="img-responsive">
                    </a>
                </div>
            </div>
        </div>
        <?php if ( is_active_sidebar( 'sidebar-widget-left' ) ) : ?>
            <?php dynamic_sidebar( 'sidebar-widget-left' ); ?>
        <?php endif ?>
    </div>
</div>