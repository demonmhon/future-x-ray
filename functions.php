<?php

    require get_template_directory() . '/inc/wp_bootstrap_navwalker.php';

    // Theme Config
    // ============================================================
    // Format
    // ============================================================
    define('THEME_CONFIG_FORMAT_DATE'         , 'M d, Y');
    // Navigations
    // ============================================================
    define('THEME_CONFIG_MAIN_MENU_LOCATION'    , 'main-menu');
    define('THEME_CONFIG_MAIN_MENU_CONTAINER'   , false);
    define('THEME_CONFIG_MAIN_MENU_ID'          , 'main-menu');
    define('THEME_CONFIG_MAIN_MENU_CLS'         , 'main-menu nav navbar-nav navbar-right');
    define('THEME_CONFIG_MAIN_MENU_DEPTH'       , 3);

    define('THEME_CONFIG_SIDE_MENU_LOCATION '   , 'side-menu');
    define('THEME_CONFIG_SIDE_MENU_CONTAINER'   , false);
    define('THEME_CONFIG_SIDE_MENU_ID'          , 'side-menu');
    define('THEME_CONFIG_SIDE_MENU_CLS'         , 'nav nav-side');
    define('THEME_CONFIG_SIDE_MENU_DEPTH'       , 1);

    define('THEME_CONFIG_FOOTER_MENU_LOCATION'  , 'main-menu');
    define('THEME_CONFIG_FOOTER_MENU_CONTAINER' , false);
    define('THEME_CONFIG_FOOTER_MENU_ID'        , 'footer-menu');
    define('THEME_CONFIG_FOOTER_MENU_CLS'       , 'footer-menu nav navbar-nav');
    define('THEME_CONFIG_FOOTER_MENU_DEPTH'     , 1);
    // Page
    // ============================================================
    define('THEME_CONFIG_PAGE_HOME'             , 'home');
    define('THEME_CONFIG_PAGE_CONTACT'          , 'contact-us');
    // Category
    // ============================================================
    define('THEME_CONFIG_CAT_INDEX_BANNER'      , 'index-banner');
    define('THEME_CONFIG_CAT_SERVICES'          , 'services');
    define('THEME_CONFIG_CAT_PRODUCTS'          , 'products');
    define('THEME_CONFIG_CAT_PROJECTS'          , 'projects');
    // Limit
    // ============================================================
    define('THEME_CONFIG_LIMIT_INDEX_BANNER'              , 10);
    define('THEME_CONFIG_LIMIT_INDEX_CAT_SERVICES'        , 20);
    define('THEME_CONFIG_LIMIT_INDEX_CAT_PRODUCTS'        , 20);
    define('THEME_CONFIG_LIMIT_INDEX_CAT_PROJECT'         , 12);
    define('THEME_CONFIG_LIMIT_INDEX_CAT_PROJECTS_SIDEBAR', 12);
    // Sidebar
    // ============================================================


    // Theme Support
    // Custom Header
    // ============================================================
    add_theme_support( 'custom-header' );
    // Post Thumbnails
    // ============================================================
    add_theme_support( 'post-thumbnails' );
    // HTML5
    // ============================================================
    add_theme_support('html5', array('search-form'));
    // Menu
    // ============================================================
    function register_my_menu() {
        register_nav_menu(THEME_CONFIG_MAIN_MENU_LOCATION,__( 'Main Menu' ));
        register_nav_menu(THEME_CONFIG_SIDE_MENU_LOCATION,__( 'Side Menu' ));
    }
    add_action( 'init', 'register_my_menu' );

    // JS Script Theme
    // ============================================================
    function theme_js_script() {
        // wp_register_script( 'theme-js-bootstrap', get_template_directory_uri() . '/js/bootstrap.min.js', array( 'jquery' ) );
        // wp_register_script( 'theme-js-jqp-colorbox', get_template_directory_uri() . '/js/jquery.colorbox.min.js', array( 'jquery' ) );
        wp_register_script( 'theme-js-script', get_template_directory_uri() . '/js/global.all.min.js', array( 'jquery' ) );
        // wp_enqueue_script( 'theme-js-bootstrap' );
        // wp_enqueue_script( 'theme-js-jqp-colorbox' );
        wp_enqueue_script( 'theme-js-script' );
    }
    add_action( 'wp_enqueue_scripts', 'theme_js_script' );

    // Widget Options
    // ============================================================
    if (function_exists('register_sidebar')) {
        register_sidebar(array(
            'name'          => __('Sidebar Left'),
            'id'            => 'sidebar-widget-left',
            'description'   => __( 'Widget on the left of the page' ),
            'before_widget' => '<div class="wp-widget widget row"><div class="col-xs-12">',
            'after_widget'  => '</div></div>',
            'before_title'  => '<h3 class="title">',
            'after_title'   => '</h3><div class="body">',
        ));
        register_sidebar(array(
            'name'          => __('Sidebar Right'),
            'id'            => 'sidebar-widget-right',
            'description'   => __( 'Widget on the right of the page' ),
            'before_widget' => '<div class="wp-widget widget row">',
            'after_widget'  => '</div>',
            'before_title'  => '<h3 class="title">',
            'after_title'   => '</h3><div class="body">',
        ));
        register_sidebar(array(
            'name'          => __('YouTube Video'),
            'id'            => 'sidebar-youtube-video',
            'description'   => __( 'YouTube embed code. The video ratio should be 4:3. Place Text widget here' ),
            'before_widget' => '',
            'after_widget'  => '</div></div>',
            'before_title'  => '<h3 class="title">',
            'after_title'   => '</h3><div class="body"><div class="embed-responsive embed-responsive-4by3">',
        ));
    }

    function current_page_url() {
        $pageURL = 'http';
        if( isset($_SERVER["HTTPS"]) ) {
            if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
        }
        $pageURL .= "://";
        if ($_SERVER["SERVER_PORT"] != "80") {
            $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
        } else {
            $pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
        }
        return $pageURL;
    }

?>
