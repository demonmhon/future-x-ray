<div class="post-list post-accessories-items post-list-thumbnail col-xs-12">
	<?php while (have_posts()) : the_post(); ?>
	<!-- Post item -->
    <div id="post-<?php the_ID(); ?>" class="col-xs-6 col-md-4 post-list-item">
        <div class="the-content row">
            <div class="content col-xs-12">
               <?php
                    $contentColumnSize = array(
                        'xs' => 12,
                        'md' => 12
                    );
                    if ( has_post_thumbnail() ) {
                        $contentColumnSize = array(
                            'xs' => 8,
                            'md' => 9
                        );
                    }
                ?>
                <div class="row">
                    <div class="featured-image post-thumbnail col-xs-12">
                        <a href="<?php the_permalink() ?>">
                        <?php if ( has_post_thumbnail() ) : ?>
                            <?php the_post_thumbnail('medium'); ?>
                        <?php else: ?>
                        <?php endif; ?>
                        </a>
                    </div>
                </div>
                <div class="row">
                    <h2 class="title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                    <div class="content col-xs-12">
                        <?php the_excerpt(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
	<?php endwhile; ?>
</div>