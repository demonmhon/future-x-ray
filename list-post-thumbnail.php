<div class="row the-post">
    <div class="post-list post-list-thumbnail col-md-12">
        <?php while (have_posts()) : the_post(); ?>
        <!-- Post item -->
        <div id="post-<?php the_ID(); ?>" <?php post_class('row'); ?>>
            <div class="col-md-12 post-list-item">
                <div class="the-content row">
                    <div class="content col-xs-12">
                        <h2 class="title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                        <?php if (!is_home() && !is_front_page()) : ?>
                        <div class="the-info">
                            <p class="the-date"><span class="date"><?php the_time(THEME_CONFIG_FORMAT_DATE); ?></span></p>
                            <p class="the-tags"><?php the_tags('แท็ก: '); ?></p>
                        </div>
                        <?php endif ?>
                        <div class="row">
                           <?php
                                $contentColumnSize = array(
                                    'xs' => 12,
                                    'md' => 12
                                );
                                if ( has_post_thumbnail() ) {
                                    $contentColumnSize = array(
                                        'xs' => 8,
                                        'md' => 9
                                    );
                                }
                            ?>
                            <?php if ( has_post_thumbnail() ) : ?>
                                <div class="featured-image post-thumbnail col-xs-4 col-md-3">
                                    <a href="<?php the_permalink() ?>">
                                        <?php the_post_thumbnail('medium'); ?>
                                    </a>
                                </div>
                            <?php endif; ?>
                            <div class="content col-xs-<?php echo $contentColumnSize['xs'] ?> col-md-<?php echo $contentColumnSize['md'] ?>">
                                <?php the_excerpt(); ?>
                            </div>
                        </div>
                        <p class="read-more"><a href="<?php the_permalink(); ?>">อ่านต่อ</a></p>
                    </div>
                </div>
            </div>
        </div>
        <?php endwhile; ?>
    </div>
</div>