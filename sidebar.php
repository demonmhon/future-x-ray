<div class="col-md-4 sidebar">
    <div class="widget row">
       <div class="col-xs-12">
            <div class="body banner">
                <?php
                    $page_args     = array('name' => THEME_CONFIG_PAGE_CONTACT, 'post_type' => 'page');
                    $contact_page  = get_posts($page_args);
                    $contact_page  = $contact_page[0];
                ?>
                <a href="<?php echo get_page_link( $contact_page->ID ) ?>">
                    <img src="<?php bloginfo('template_url'); ?>/img/banner/contact-us.png" alt="Contact us banner" class="img-responsive">
                </a>
            </div>
        </div>
    </div>
    <div class="widget recent-project row">
        <div class="col-xs-12">
            <h3 class="title">Recent Projects</h3>
            <div class="body">
               <?php
                    $index_content_products = query_posts(
                        array(
                            category_name  => THEME_CONFIG_CAT_PROJECTS,
                            posts_per_page => THEME_CONFIG_LIMIT_INDEX_CAT_PROJECTS_SIDEBAR
                        )
                    );
                    $post_count = $wp_query->found_posts;
                    if ( have_posts() ):
                        while ( have_posts() ) : the_post();
                ?>
                <div class="item">
                    <a href="<?php the_permalink() ?>" class="preview">
                        <span class="img-wrapper">
                           <?php if ( has_post_thumbnail() ) : ?>
                                <?php the_post_thumbnail('medium'); ?>
                            <?php endif ?>
                        </span>
                        <span class="caption"><?php the_title() ?></span>
                    </a>
                </div>
                <?php
                        endwhile;
                    endif;
                ?>
            </div>
        </div>
    </div>
    <?php if ( is_active_sidebar( 'sidebar-youtube-video' ) ) : ?>
    <!-- YouTube Video -->
    <div class="widget video row">
        <div class="col-xs-12">
            <?php dynamic_sidebar( 'sidebar-youtube-video' ); ?>
        </div>
    </div>
    <?php endif; ?>
    
    <div class="widget links row government-office">
        <div class="col-xs-12">
            <h3 class="title">Links</h3>
            <div class="body">
                <ul class="list-unstyled">
                    <li>
                        <a href="http://www.moph.go.th/moph2/index4.php">
                            <img src="<?php bloginfo('template_url'); ?>/img/logo/logo-moph.png" alt="" class="img-responsive">
                            <span>กระทรวงสาธารณสุข</span>
                        </a>
                    </li>
                    <li>
                        <a href="http://www.dmsc.moph.go.th/dmsc/home.php">
                            <img src="<?php bloginfo('template_url'); ?>/img/logo/logo-dmsc.png" alt="" class="img-responsive">
                            <span>กรมวิทยาศาสตร์การแพทย์</span>
                        </a>
                    </li>
                    <li>
                        <a href="http://www.oaep.go.th/">
                            <img src="<?php bloginfo('template_url'); ?>/img/logo/logo-oaep.png" alt="" class="img-responsive">
                            <span>สำนักปรมณูเพื่อสันติ</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar() ) : ?>
    <?php endif ?>
    <?php if ( is_active_sidebar( 'sidebar-widget-right' ) ) : ?>
        <?php dynamic_sidebar( 'sidebar-widget-right' ); ?>
    <?php endif ?>
    <?php if (is_single()) : ?>
        
    <?php endif ?>
</div>