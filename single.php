<?php get_header(); ?>
<?php if (have_posts()) : the_post() ?>
<div class="row post-single">
    <div class="col-xs-12 col-md-9 main-content-body">
       <?php
            $single_width_class = 'col-md-8';
        ?>
        <!-- Post: <?php the_ID() ?> -->
        <div id="post-<?php the_ID(); ?>" class="<?php echo $single_width_class; ?>">
            <?php get_template_part('post') ?>
        </div>
        <?php get_sidebar(); ?>
    </div>
    <?php get_template_part('main-nav') ?>
</div>
<?php else : ?>
<?php endif; ?>
<?php get_footer(); ?>